[![GitHub tag](https://img.shields.io/github/tag/netravnen/CiscoLabCaseStudies.svg)](https://gitlab.com/netravnen/CiscoLabCaseStudies)
[![license](https://img.shields.io/github/license/netravnen/CiscoLabCaseStudies.svg)](https://gitlab.com/netravnen/CiscoLabCaseStudies/blob/master/LICENSE.md)
[![GitHub commits](https://img.shields.io/github/commits-since/netravnen/CiscoLabCaseStudies/v1.1.1.svg)](https://gitlab.com/netravnen/CiscoLabCaseStudies/commits/master)

# URLs
- [Gitlab][1]

[1]: https://gitlab.com/netravnen/CiscoLabCaseStudies
[2]: https://github.com/netravnen/CiscoLabCaseStudies