# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]
### Added
### Changed

## [1.1.1] - 2017-06-06
### Changed
- Note vlan 66 is to be used as internal network mgmt vlan.

## [1.1.0] - 2017-06-06
### Added
- Routing protocol ipv6 capable
- HSRP interface tracking
- Set voice vlan for at least 1 port

## [1.0.0] - 2017-06-06
### Added
- Initial version

[Unreleased]: https://gitlab.com/netravnen/CiscoLabCaseStudies/compare/v1.1.1...HEAD
[1.1.1]: https://gitlab.com/netravnen/CiscoLabCaseStudies/compare/v1.1.0...v1.1.1
[1.1.0]: https://gitlab.com/netravnen/CiscoLabCaseStudies/compare/v1.0.0...v1.1.0
[1.0.0]: https://gitlab.com/netravnen/CiscoLabCaseStudies/compare/f83a78...v1.0.0
